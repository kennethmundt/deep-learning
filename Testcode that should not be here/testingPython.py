""" For testing various elements of Python """

########################## lambdas #############################

# Simple lambda expression
x = lambda a : a + 10

# More coplex lambda

def myfunc(n):
    return lambda a : a * n

# This is very complexity starts.
# myDoubler becomes myfunc() with a function parameter of 2 given i.e. n = 2
# mydoubler become the following: lambda a : a * 2
myDoubler = myfunc(2)


#################################################################




if __name__ == "__main__":
    print(x(5))

    # Then when you call this the "11" becomes "a" in the function and since the lambda is now "lambda a : a * 2" the result becomes 22 (double 11).
    # At first very complex, but actually not too bad once you are able to wrap your head around it.
    print(myDoubler(11))
    