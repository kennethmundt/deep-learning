"""
Basic Neural Network for predicting outcome (1 or 0) based on input A, B, C.
"""
import numpy as np

class NeuralNetwork():

    def __init__(self):

        np.random.seed(1) # Seed is arbitrary and should only have a set value if you want to control the outcome of random().
        self.synaptic_weights = 2 * np.random.random((3,1)) - 1 # Set random weights.
        # self.synaptic_weights = np.random.random((3,1)) - 1 This seems to be working just as well
        
    # Activation function that ensures and output between 0 and 1    
    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    # Calculate the gradient
    def sigmoid_derivative(self, x):
        return x * (1 - x)

    # Takes care of predicting a the output i.e. Forward propagation
    def make_prediction(self, input):
        input = input.astype(float)
        output = self.sigmoid(np.dot(input, self.synaptic_weights))
        return output

    # Train the neural net.
    def train(self, training_input, training_output, training_iterations):

        for iteration in range(training_iterations):

            output = self.make_prediction(training_input)
            error = training_output - output
            adjustments = np.dot(training_input.T, error * self.sigmoid_derivative(output))
            self.synaptic_weights += adjustments


if __name__ == "__main__":
    neural_network = NeuralNetwork()

    print('Random synaptic weights')
    print(neural_network.synaptic_weights)

    # training data
    training_input = np.array([[0,0,1], # X1
                            [1,1,1], # X2
                            [1,0,1], # X3
                            [0,1,1]])# X4

    # training output i.e. the outcome of the traning data in the "real world". Remember, this i supervised learning.
    # meaning that we tell the algorithm what the outcome of some input is and then the algo uses that to make its predictions
    training_output = np.array([[0,1,1,0]]).T 

    neural_network.train(training_input, training_output, 10000) # Training step

    print('synaptic weights after training')
    print(neural_network.synaptic_weights)

    print('Stop application with ctrl + c. If you are on a Mac.. You do what you gotta do!')
    while True:
        print('Enter either a "1" or a "0"')
        A = str(input('Input 1: '))
        B = str(input('Input 2: '))
        C = str(input('Input 3: '))

        print('Output data: ')
        print(neural_network.make_prediction(np.array([A, B, C])))
