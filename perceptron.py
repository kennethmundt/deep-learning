import numpy as np

# Sigmoid activation funtion
def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_derivative(x):
    return x * (1 - x)


# Never forgot double brackets when declaring np-arrays.
# Difference between 1x [] and 2x [[]] is dimensions which becomes important
# when doing vectorization. Try wrapping training_inputs in np.shape() with both [] & [[]]
training_inputs = np.array([[0,0,1,1,1,0,1,0], # X1
                            [1,1,1,1,0,0,0,1], # X2
                            [1,0,1,0,0,1,1,0], # X3
                            [0,1,1,1,0,1,0,1]])# X4

# Transpose (.T) switches the dimensions of the array i.e. 3X4 becomes 4X3.
# Below transpose transforms [0,1,1,0] -> [0, W1
#                                          1, W2
#                                          1, W3
#                                          0,]W4
# You can only multiply matrices if the number of columns in the first matrix
# is equal to the number of rows in the second. Hence the transpose (T).  
training_outputs = np.array([[0,1,1,0]]).T 
##### print(training_outputs) #####

# Using the same seed will give you the same random value every time.
# when you call random.rand() after
np.random.seed(1) 

# Creating a 3x1 matrix because we have 3 input values per training example
synaptic_weights = 2 * np.random.random((8,1)) - 1

print('Random synaptic starting weights:')
print(synaptic_weights)

# This is a little redundant since neither of the values ever change.
# one could just use training_inputs instead
input_layer = training_inputs 

for i in range(100):

    outputs = sigmoid(np.dot(input_layer, synaptic_weights)) # Make the prediction

    error = training_outputs - outputs # record the error/cost

    adjustments = error * sigmoid_derivative(outputs) # Calculate the adjustments based on the error

    synaptic_weights += np.dot(input_layer.T, adjustments) # Update the weights

print('Synaptic weights after training')
print(synaptic_weights)



print("Outputs after training")
print(outputs)

